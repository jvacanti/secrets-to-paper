secrets-to-paper
----------------

..
  comment goes here



Synopsis
~~~~~~~~

stp [OPTIONS] COMMAND [ARGS]...


Description
~~~~~~~~~~~

A command-line tool to convert secret keys to printable PDFs and to parse those
PDFs back to usable secret keys. Note: Python 3.8+ is required to use this
package. Python 3.8 introduced a new computation for modular inverses.


Options
~~~~~~~

--debug / --no-debug

--help  Show this message and exit.


Commands
~~~~~~~~

.. glossary::

    export
      Helper functions for writing secret keys.

    export-gpg
      Helper function to generate archive of GPG keys.

    gen-ecc
      Helper function to generate ECC private key from A, B, and D.

    gen-rsa
      Helper function to generate RSA private key from P and Q.

    parse
      Helper functions to parse secret keys into PEM format.


Environment
~~~~~~~~~~~

You can adjust the working ``GNUPGP`` directory by setting the environment
variable.


Bugs
~~~~

No known bugs (i.e. perfect software)


Notes
~~~~~

paperkey and weasyprint are dependencies of this project.


Examples
~~~~~~~~

Some examples of common usage.

