FROM python

RUN apt-get -y update && \
    apt-get -y install libgl1 libzbar0

RUN pip install opencv-python opencv-contrib-python numpy pyzbar evdev

COPY dist /tmp/dist
RUN pip install --find-links /tmp/dist/ secrets-to-paper

CMD ["stp"]
